package com.sweaterweather.root

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.sweaterweather.R
import com.sweaterweather.databinding.ActivityRootBinding
import com.sweaterweather.mainpage.ui.MainPageFragment
import com.sweaterweather.utils.extensions.popFeature

class RootActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRootBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRootBinding.inflate(layoutInflater)
        setContentView(binding.root)
        replace(MainPageFragment())
    }

    @Deprecated(
        "Deprecated in Java",
        ReplaceWith("popFeature()", "com.sweaterweather.utils.extensions.popFeature")
    )
    override fun onBackPressed() {
        popFeature()
    }

    private fun replace(fragment: Fragment) {
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }
}

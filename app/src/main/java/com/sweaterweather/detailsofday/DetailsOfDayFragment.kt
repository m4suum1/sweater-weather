package com.sweaterweather.detailsofday

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.sweaterweather.R
import com.sweaterweather.common.mvp.BaseFragment
import com.sweaterweather.databinding.FragmentDetailsOfDayBinding
import com.sweaterweather.fivedayspage.model.FiveDaysWeather
import com.sweaterweather.utils.Arguments
import com.sweaterweather.utils.extensions.dateFormat
import com.sweaterweather.utils.extensions.getDrawableIcon
import com.sweaterweather.utils.extensions.popScreen
import com.sweaterweather.utils.extensions.viewbinding.viewBinding

class DetailsOfDayFragment : BaseFragment(R.layout.fragment_details_of_day) {

    private val binding: FragmentDetailsOfDayBinding by viewBinding()

    companion object {
        fun newInstance(data: FiveDaysWeather) = DetailsOfDayFragment().apply {
            arguments = bundleOf(Arguments.FIVE_DAYS_WEATHER_LIST to data)
        }
    }

    private val data: FiveDaysWeather? by lazy {
        arguments?.getParcelable(Arguments.FIVE_DAYS_WEATHER_LIST)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val description = data?.description.toString()
        val date = dateFormat(data?.dtTxt.toString())
        val temperature = data?.temp
        val temperatureFeelsLike = data?.feelsLike
        val temperatureMin = data?.tempMin
        val temperatureMax = data?.tempMax
        val pressure = data?.pressure
        val humidity = data?.humidity
        with(binding) {
            imageViewIcon.setImageResource(getDrawableIcon(description))
            textViewDescriptionOfDay.text = description
            toolBarDetailsOfDayPageTitle.title = date
            textViewTemperature.text = temperature
            textViewFeelsLike.text = temperatureFeelsLike
            textViewTempMin.text = temperatureMin
            textViewTempMax.text = temperatureMax
            textViewPressure.text = pressure
            textViewHumidity.text = humidity
            toolBarDetailsOfDayPageTitle.setNavigationOnClickListener {
                popScreen()
            }
        }
    }

}
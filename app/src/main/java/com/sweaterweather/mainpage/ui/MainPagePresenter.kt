package com.sweaterweather.mainpage.ui

import com.sweaterweather.common.mvp.BasePresenter
import com.sweaterweather.mainpage.interactor.MainPageInteractor
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch

class MainPagePresenter(
    private val interactor: MainPageInteractor
) : BasePresenter<MainPageContract.View>(), MainPageContract.Presenter {
    override fun getWeatherData(cityName: String, appidKey: String, metric: String) {
        launch {
            try {
                view?.showLoading(isLoading = true)
                val weatherData = interactor.getWeatherData(cityName, appidKey, metric)
                weatherData.let { weather ->
                    if (weather != null) {
                        view?.changeIcon(weather)
                        view?.showWeatherData(weather)
                    }
                }
            } catch (t: Throwable) {
                view?.showError(t.message.toString())
            } catch (e: CancellationException) {
                view?.showError(e.message.toString())
            } finally {
                view?.showLoading(isLoading = false)
            }
        }
    }
}
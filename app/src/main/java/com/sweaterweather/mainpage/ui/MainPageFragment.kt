package com.sweaterweather.mainpage.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.sweaterweather.R
import com.sweaterweather.common.mvp.BaseMvpFragment
import com.sweaterweather.databinding.FragmentWeatherMainPageBinding
import com.sweaterweather.detailpage.WeatherDetailedPageFragment
import com.sweaterweather.mainpage.model.WeatherData
import com.sweaterweather.utils.extensions.getDrawableIcon
import com.sweaterweather.utils.extensions.replaceScreen
import com.sweaterweather.utils.extensions.viewbinding.viewBinding
import org.koin.android.ext.android.inject
import timber.log.Timber

private const val TEXT_MIN_SIZE = 3
private const val TEXT_MAX_SIZE = 20

class MainPageFragment :
    BaseMvpFragment<MainPageContract.View, MainPageContract.Presenter>(R.layout.fragment_weather_main_page),
    MainPageContract.View {
    override val presenter: MainPagePresenter by inject()
    private val binding: FragmentWeatherMainPageBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val appidKey = resources.getString(R.string.open_weather_key)
        val celsiusUnits = resources.getString(R.string.celsius_units)

        with(binding) {
            textInputEditTextEnterCity.doAfterTextChanged {
                if (it?.length in TEXT_MIN_SIZE..TEXT_MAX_SIZE) {
                    textInputLayout.isErrorEnabled = false
                    val cityName = it.toString()
                    presenter.getWeatherData(
                        cityName = cityName,
                        appidKey = appidKey,
                        metric = celsiusUnits
                    )
                } else if (it?.length!! < 3) {
                    textInputLayout.error =
                        resources.getString(R.string.text_input_layout_error_min)
                    textInputLayout.isErrorEnabled = true
                } else {
                    textInputLayout.error =
                        resources.getString(R.string.text_input_layout_error_max)
                    textInputLayout.isErrorEnabled = true
                }
            }
        }
    }


    override fun showWeatherData(data: WeatherData) {
        with(binding) {
            changeWeatherDataVisibility()
            textViewCityName.text = data.name
            textViewTemperature.text = data.temp
            buttonDetailedInfo.setOnClickListener {
                replaceScreen(WeatherDetailedPageFragment.newInstance(data))
            }
        }
    }

    override fun showError(message: String) {
        Timber.i("ERROR --->>> $message")
    }

    override fun showLoading(isLoading: Boolean) {
        binding.progress.isVisible = isLoading
    }

    override fun changeIcon(data: WeatherData) {
        binding.imageViewIcon.setImageResource(getDrawableIcon(data.description))
    }

    private fun changeWeatherDataVisibility() {
        with(binding) {
            textViewCityName.visibility = View.VISIBLE
            textViewTemperature.visibility = View.VISIBLE
            buttonDetailedInfo.visibility = View.VISIBLE
        }
    }
}
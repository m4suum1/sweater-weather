package com.sweaterweather.mainpage.ui

import com.sweaterweather.common.mvp.MvpPresenter
import com.sweaterweather.common.mvp.MvpView
import com.sweaterweather.mainpage.model.WeatherData

interface MainPageContract {
    interface View : MvpView {
        fun showWeatherData(data: WeatherData)
        fun showError(message: String)
        fun showLoading(isLoading: Boolean)

        fun changeIcon(data: WeatherData)
    }

    interface Presenter : MvpPresenter<View> {
        fun getWeatherData(cityName: String, appidKey: String, metric: String)
    }
}

package com.sweaterweather.mainpage.repository

import com.sweaterweather.mainpage.model.WeatherData

interface MainPageRepository {
    suspend fun getWeatherData(cityName: String, appidKey: String, metric: String): WeatherData?
}
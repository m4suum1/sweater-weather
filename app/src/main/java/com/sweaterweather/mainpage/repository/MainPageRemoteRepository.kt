package com.sweaterweather.mainpage.repository

import com.sweaterweather.mainpage.api.MainPageApi
import com.sweaterweather.mainpage.model.MainPageConverter

class MainPageRemoteRepository(val api: MainPageApi) : MainPageRepository {
    override suspend fun getWeatherData(
        cityName: String,
        appidKey: String,
        metric: String
    ) = MainPageConverter.fromNetwork(
        api.getWeatherData(
            cityName = cityName,
            appidKey = appidKey,
            metric = metric
        )
    )
}
package com.sweaterweather.mainpage.api.model

import com.google.gson.annotations.SerializedName

data class CloudsResponse(
    @SerializedName("all")
    val all: Int?
)
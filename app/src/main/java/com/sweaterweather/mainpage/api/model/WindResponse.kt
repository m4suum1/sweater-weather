package com.sweaterweather.mainpage.api.model

import com.google.gson.annotations.SerializedName

data class WindResponse(
    @SerializedName("speed")
    val speed: Float?,
    @SerializedName("deg")
    val deg: Int?,
    @SerializedName("gust")
    val gust: Float?
)
package com.sweaterweather.mainpage.di

import com.sweaterweather.common.di.InjectionModule
import com.sweaterweather.mainpage.api.MainPageApi
import com.sweaterweather.mainpage.interactor.MainPageInteractor
import com.sweaterweather.mainpage.repository.MainPageRemoteRepository
import com.sweaterweather.mainpage.repository.MainPageRepository
import com.sweaterweather.mainpage.ui.MainPagePresenter
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object MainPageModule : InjectionModule {

    override fun create() = module {
        single { get<Retrofit>().create(MainPageApi::class.java) } //private val api: WeatherApi = RetrofitClient.getClient().create(WeatherApi::class.java)
        single<MainPageRepository> { MainPageRemoteRepository(get()) } //-> creating Interface implementation
        single { MainPageRemoteRepository(get()) } bind MainPageRepository::class //private val remoteRepository = MainPageRemoteRepository(api)
        factory { MainPageInteractor(get()) } //private val interactor = MainPageInteractor(remoteRepository)

        single { MainPagePresenter(get()) } // override val presenter: MainPagePresenter = MainPagePresenter(interactor)
    }

}

package com.sweaterweather.mainpage.interactor

import com.sweaterweather.mainpage.model.WeatherData
import com.sweaterweather.mainpage.repository.MainPageRemoteRepository

class MainPageInteractor(private val remoteRepository: MainPageRemoteRepository) {
    suspend fun getWeatherData(cityName: String, appidKey: String, metric: String): WeatherData? =
        remoteRepository.getWeatherData(cityName, appidKey, metric)
}
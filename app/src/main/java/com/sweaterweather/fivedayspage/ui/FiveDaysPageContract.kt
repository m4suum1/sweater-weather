package com.sweaterweather.fivedayspage.ui

import com.sweaterweather.common.mvp.MvpPresenter
import com.sweaterweather.common.mvp.MvpView
import com.sweaterweather.fivedayspage.model.FiveDaysWeather

interface FiveDaysPageContract {

    interface View : MvpView {
        fun showLoading(isLoading: Boolean)
        fun showError(error: String)
        fun showData(data: List<FiveDaysWeather>)
    }

    interface Presenter : MvpPresenter<View> {
        fun getWeatherData(cityName: String, appidKey: String, metric: String)
    }
}
package com.sweaterweather.fivedayspage.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sweaterweather.databinding.ItemWeatherBinding
import com.sweaterweather.fivedayspage.model.FiveDaysWeather
import com.sweaterweather.utils.extensions.dateFormat
import com.sweaterweather.utils.extensions.getDrawableIcon

class FiveDaysPageViewHolder(
    private val binding: ItemWeatherBinding,
    private val clickOnItem: (FiveDaysWeather) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup,
        onClickItem: (FiveDaysWeather) -> Unit
    ) : this(
        ItemWeatherBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        onClickItem
    )

    private lateinit var description: String
    private lateinit var temperature: String
    private lateinit var date: String
    private lateinit var item: FiveDaysWeather

    fun getData(item: FiveDaysWeather) {
        description = item.description
        temperature = item.temp
        date = dateFormat(item.dtTxt)
        this.item = item
    }

    fun onBind() {
        with(binding) {
            imageViewIconItem.setImageResource(getDrawableIcon(description))
            textViewTemperature.text = temperature
            textViewDate.text = date
        }
        itemView.setOnClickListener {
            clickOnItem(item)
        }
    }
}

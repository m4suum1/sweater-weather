package com.sweaterweather.fivedayspage.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.sweaterweather.R
import com.sweaterweather.common.mvp.BaseMvpFragment
import com.sweaterweather.databinding.FragmentFiveDaysWeatherBinding
import com.sweaterweather.detailsofday.DetailsOfDayFragment
import com.sweaterweather.fivedayspage.model.FiveDaysWeather
import com.sweaterweather.fivedayspage.ui.adapter.FiveDaysPageAdapter
import com.sweaterweather.utils.Arguments.CITY_NAME
import com.sweaterweather.utils.extensions.args
import com.sweaterweather.utils.extensions.popScreen
import com.sweaterweather.utils.extensions.replaceScreen
import com.sweaterweather.utils.extensions.viewbinding.viewBinding
import com.sweaterweather.utils.extensions.withArgs
import org.koin.android.ext.android.inject
import timber.log.Timber

class FiveDaysPageFragment :
    BaseMvpFragment<FiveDaysPageContract.View, FiveDaysPageContract.Presenter>(R.layout.fragment_five_days_weather),
    FiveDaysPageContract.View {

    companion object {
        fun newInstance(cityName: String) = FiveDaysPageFragment()
            .withArgs(CITY_NAME to cityName)
    }

    private val cityName: String? by args(CITY_NAME)

    override val presenter: FiveDaysPagePresenter by inject()

    private val binding: FragmentFiveDaysWeatherBinding by viewBinding()

    private val adapter: FiveDaysPageAdapter by lazy {
        FiveDaysPageAdapter { item ->
            replaceScreen(DetailsOfDayFragment.newInstance(item))
        }
    }

    private val layoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val appidKey = getString(R.string.open_weather_key)
        val celsiusUnits = getString(R.string.celsius_units)

        presenter.getWeatherData(
            cityName = cityName.toString(),
            appidKey = appidKey,
            metric = celsiusUnits
        )

        with(binding) {
            toolBar.title = cityName
            recyclerViewFiveDaysWeather.layoutManager = layoutManager
            recyclerViewFiveDaysWeather.setHasFixedSize(true)
            recyclerViewFiveDaysWeather.adapter = adapter
            adapter.onAttachedToRecyclerView(recyclerViewFiveDaysWeather)

            toolBar.setNavigationOnClickListener {
                popScreen()
            }

        }
    }


    override fun showLoading(isLoading: Boolean) {
        binding.progressFiveDays.isVisible = isLoading
    }

    override fun showError(error: String) {
        Timber.i("ERROR --->>> $error")
    }

    override fun showData(data: List<FiveDaysWeather>) {
        adapter.setData(data)
    }

}
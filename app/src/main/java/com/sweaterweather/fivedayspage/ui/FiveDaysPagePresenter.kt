package com.sweaterweather.fivedayspage.ui

import com.sweaterweather.common.mvp.BasePresenter
import com.sweaterweather.fivedayspage.interactor.FiveDaysPageInteractor
import kotlinx.coroutines.launch

class FiveDaysPagePresenter(
    private val interactor: FiveDaysPageInteractor
) : BasePresenter<FiveDaysPageContract.View>(), FiveDaysPageContract.Presenter {

    override fun getWeatherData(cityName: String, appidKey: String, metric: String) {
        launch {
            try {
                view?.showLoading(isLoading = true)
                val data = interactor.getWeatherData(cityName, appidKey, metric)
                val list = data.list
                list.let { view?.showData(it) }
            } catch (t: Throwable) {
                view?.showError(t.message.toString())
            } finally {
                view?.showLoading(false)
            }
        }
    }
}
package com.sweaterweather.fivedayspage.di

import com.sweaterweather.common.di.InjectionModule
import com.sweaterweather.fivedayspage.api.FiveDaysPageWeatherApi
import com.sweaterweather.fivedayspage.interactor.FiveDaysPageInteractor
import com.sweaterweather.fivedayspage.repository.FiveDaysPageRemoteRepository
import com.sweaterweather.fivedayspage.repository.FiveDaysPageRepository
import com.sweaterweather.fivedayspage.ui.FiveDaysPagePresenter
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object FiveDaysModule : InjectionModule {
    override fun create() = module {
        single { get<Retrofit>().create(FiveDaysPageWeatherApi::class.java) }
        single<FiveDaysPageRepository> { FiveDaysPageRemoteRepository(get()) }
        single { FiveDaysPageRemoteRepository(get()) } bind FiveDaysPageRepository::class
        factoryOf(::FiveDaysPageInteractor)

        single { FiveDaysPagePresenter(get()) }
    }
}
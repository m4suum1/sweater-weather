package com.sweaterweather.fivedayspage.repository

import com.sweaterweather.fivedayspage.api.FiveDaysPageWeatherApi
import com.sweaterweather.fivedayspage.model.DetailWeatherData
import com.sweaterweather.fivedayspage.model.FiveDaysPageConverter

class FiveDaysPageRemoteRepository(val api: FiveDaysPageWeatherApi) : FiveDaysPageRepository {

    override suspend fun getFiveDaysWeatherData(
        cityName: String,
        appidKey: String,
        metric: String
    ): DetailWeatherData {
        val data = api.getFiveDaysWeatherData(cityName, appidKey, metric)
        return FiveDaysPageConverter.fromNetwork(data)
    }
}
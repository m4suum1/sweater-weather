package com.sweaterweather.fivedayspage.repository

import com.sweaterweather.fivedayspage.model.DetailWeatherData

interface FiveDaysPageRepository {
  suspend  fun getFiveDaysWeatherData(
        cityName: String,
        appidKey: String,
        metric: String): DetailWeatherData?
}
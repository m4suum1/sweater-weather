package com.sweaterweather.fivedayspage.api.model

import com.google.gson.annotations.SerializedName
import com.sweaterweather.mainpage.api.model.CoordResponse

data class DetailCityResponse(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("coord")
    val coord: CoordResponse,
    @SerializedName("country")
    val country: String,
    @SerializedName("population")
    val population: Long,
    @SerializedName("timezone")
    val timezone: Long,
    @SerializedName("sunrise")
    val sunrise: Long,
    @SerializedName("sunset")
    val sunset: Long
)

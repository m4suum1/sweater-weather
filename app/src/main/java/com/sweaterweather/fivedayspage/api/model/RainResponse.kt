package com.sweaterweather.fivedayspage.api.model

import com.google.gson.annotations.SerializedName

data class RainResponse (
    @SerializedName("3h")
    val the3H: Double?
)
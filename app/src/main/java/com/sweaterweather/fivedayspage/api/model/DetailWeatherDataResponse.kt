package com.sweaterweather.fivedayspage.api.model

import com.google.gson.annotations.SerializedName

data class DetailWeatherDataResponse(
    @SerializedName("cod")
    val cod: String?,
    @SerializedName("message")
    val message: Int?,
    @SerializedName("cnt")
    val cnt: Long?,
    @SerializedName("list")
    val list: List<FiveDaysWeatherResponse>,
    @SerializedName("city")
    val city: DetailCityResponse?

)
package com.sweaterweather.fivedayspage.model

import com.sweaterweather.fivedayspage.api.model.DetailWeatherDataResponse
import com.sweaterweather.fivedayspage.api.model.FiveDaysWeatherResponse
import com.sweaterweather.mainpage.api.model.MainResponse
import com.sweaterweather.mainpage.api.model.WeatherResponse
import com.sweaterweather.utils.extensions.tempDescription

object FiveDaysPageConverter {

    fun fromNetwork(response: DetailWeatherDataResponse): DetailWeatherData {
        return DetailWeatherData(
            list = fromNetwork(response.list)
        )
    }

    private fun fromNetwork(response: List<FiveDaysWeatherResponse>): List<FiveDaysWeather> {
        return response.map { data ->
            FiveDaysWeather(
                temp = fromNetworkTemp(data.main),
                feelsLike = fromNetworkFeelsLike(data.main),
                tempMin = fromNetworkTempMin(data.main),
                tempMax = fromNetworkTempMax(data.main),
                pressure = fromNetworkPressure(data.main),
                humidity = fromNetworkHumidity(data.main),
                description = fromNetworkDescription(data.weather),
                dtTxt = data.dtTxt
            )
        }
    }

    private fun fromNetworkTemp(response: MainResponse): String {
        return tempDescription(response.temp)
    }

    private fun fromNetworkFeelsLike(response: MainResponse): String {
        return tempDescription(response.feelsLike)
    }

    private fun fromNetworkTempMin(response: MainResponse): String {
        return tempDescription(response.tempMin)
    }

    private fun fromNetworkTempMax(response: MainResponse): String {
        return tempDescription(response.tempMax)
    }

    private fun fromNetworkPressure(response: MainResponse): String {
        return response.pressure.toString()
    }

    private fun fromNetworkHumidity(response: MainResponse): String {
        return response.humidity.toString()
    }

    private fun fromNetworkDescription(response: List<WeatherResponse>): String {
        return response[0].description.toString().replaceFirstChar { it.uppercase() }
    }

}
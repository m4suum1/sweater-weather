package com.sweaterweather.fivedayspage.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FiveDaysWeather(
    val temp: String,
    val feelsLike: String,
    val tempMin: String,
    val tempMax: String,
    val pressure: String,
    val humidity: String,
    val description: String,
    val dtTxt: String
) : Parcelable


package com.sweaterweather.fivedayspage.interactor

import com.sweaterweather.fivedayspage.repository.FiveDaysPageRemoteRepository

class FiveDaysPageInteractor(private val remoteRepository: FiveDaysPageRemoteRepository) {
    suspend fun getWeatherData(cityName: String, appidKey: String, metric: String) =
        remoteRepository.getFiveDaysWeatherData(cityName, appidKey, metric)
}
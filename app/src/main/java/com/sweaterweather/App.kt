package com.sweaterweather

import android.app.Application
import com.sweaterweather.mainpage.di.MainPageModule
import com.sweaterweather.common.di.NetworkModule
import com.sweaterweather.fivedayspage.di.FiveDaysModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.context.GlobalContext.stopKoin
import timber.log.Timber


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()
        setupKoin()
    }

    private fun setupTimber() {
        Timber.plant(Timber.DebugTree())
    }

    private fun setupKoin() {
        stopKoin()
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    NetworkModule.create(),
                    MainPageModule.create(),
                    FiveDaysModule.create()
                )
            )
        }
    }
}
package com.sweaterweather.utils.extensions

import androidx.fragment.app.Fragment
import com.sweaterweather.R
import com.sweaterweather.fivedayspage.ui.adapter.FiveDaysPageEnumWeatherDescriptionType
import timber.log.Timber
import kotlin.math.roundToInt

fun dateFormat(date: String) = buildString {
    append(date.split(" ")[0].split("-")[2])
        .append(".")
        .append(date.split(" ")[0].split("-")[1])
        .append("  ")
        .append(date.split(" ")[1].split(":")[0])
        .append(":")
        .append(date.split(" ")[1].split(":")[1])
}

//Пример dtTxt = 2023-04-17 06:00:00

fun tempDescription(temp: Float?): String {
    val descriptionOfTemp = " °C"
    val tempData = temp?.roundToInt()
    return buildString {
        append(tempData)
            .append(descriptionOfTemp)
    }
}

fun getDrawableIcon(type: String): Int =
    when (type) {
        FiveDaysPageEnumWeatherDescriptionType.CLEAR_SKY.description -> R.drawable.ic_clear_sky
        FiveDaysPageEnumWeatherDescriptionType.FEW_CLOUDS.description -> R.drawable.ic_few_clouds
        FiveDaysPageEnumWeatherDescriptionType.BROKEN_CLOUDS.description -> R.drawable.ic_broken_clouds
        FiveDaysPageEnumWeatherDescriptionType.SCATTERED_CLOUDS.description -> R.drawable.ic_scattered_clouds
        FiveDaysPageEnumWeatherDescriptionType.OVERCAST_CLOUDS.description -> R.drawable.ic_overcast_clouds

        FiveDaysPageEnumWeatherDescriptionType.LIGHT_INTENSITY_SHOWER_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.LIGHT_RAIN.description -> R.drawable.ic_light_rain

        FiveDaysPageEnumWeatherDescriptionType.MODERATE_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.HEAVY_INTENSITY_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.VERY_HEAVY_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.SHOWER_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.RAGGED_SHOWER_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.HEAVY_INTENSITY_SHOWER_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.EXTREME_RAIN.description -> R.drawable.ic_heavy_rain

        FiveDaysPageEnumWeatherDescriptionType.FREEZING_RAIN.description -> R.drawable.ic_freezing_rain

        FiveDaysPageEnumWeatherDescriptionType.THUNDERSTORM_WITH_LIGHT_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.THUNDERSTORM_WITH_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.THUNDERSTORM_WITH_HEAVY_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.LIGHT_THUNDERSTORM.description,
        FiveDaysPageEnumWeatherDescriptionType.THUNDERSTORM.description,
        FiveDaysPageEnumWeatherDescriptionType.HEAVY_THUNDERSTORM.description,
        FiveDaysPageEnumWeatherDescriptionType.RAGGED_THUNDERSTORM.description,
        FiveDaysPageEnumWeatherDescriptionType.THUNDERSTORM_WITH_LIGHT_DRIZZLE.description,
        FiveDaysPageEnumWeatherDescriptionType.THUNDERSTORM_WITH_DRIZZLE.description,
        FiveDaysPageEnumWeatherDescriptionType.THUNDERSTORM_WITH_HEAVY_DRIZZLE.description -> R.drawable.ic_thunderstorm

        FiveDaysPageEnumWeatherDescriptionType.LIGHT_SHOWER_SNOW.description,
        FiveDaysPageEnumWeatherDescriptionType.LIGHT_SNOW.description,
        FiveDaysPageEnumWeatherDescriptionType.SNOW.description,
        FiveDaysPageEnumWeatherDescriptionType.HEAVY_SNOW.description,
        FiveDaysPageEnumWeatherDescriptionType.SLEET.description,
        FiveDaysPageEnumWeatherDescriptionType.LIGHT_SHOWER_SLEET.description,
        FiveDaysPageEnumWeatherDescriptionType.SHOWER_SLEET.description -> R.drawable.ic_snow

        FiveDaysPageEnumWeatherDescriptionType.LIGHT_RAIN_AND_SNOW.description,
        FiveDaysPageEnumWeatherDescriptionType.RAIN_AND_SNOW.description,
        FiveDaysPageEnumWeatherDescriptionType.SHOWER_SNOW.description,
        FiveDaysPageEnumWeatherDescriptionType.HEAVY_SHOWER_SNOW.description -> R.drawable.ic_snow_and_rain

        FiveDaysPageEnumWeatherDescriptionType.MIST.description -> R.drawable.ic_mist

        FiveDaysPageEnumWeatherDescriptionType.SMOKE.description,
        FiveDaysPageEnumWeatherDescriptionType.HAZE.description,
        FiveDaysPageEnumWeatherDescriptionType.FOG.description -> R.drawable.ic_fog

        FiveDaysPageEnumWeatherDescriptionType.LIGHT_INTENSITY_DRIZZLE.description,
        FiveDaysPageEnumWeatherDescriptionType.DRIZZLE.description,
        FiveDaysPageEnumWeatherDescriptionType.HEAVY_INTENSITY_DRIZZLE.description,
        FiveDaysPageEnumWeatherDescriptionType.LIGHT_INTENSITY_DRIZZLE_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.DRIZZLE_RAIN.description -> R.drawable.ic_drizzle

        FiveDaysPageEnumWeatherDescriptionType.HEAVY_INTENSITY_DRIZZLE_RAIN.description,
        FiveDaysPageEnumWeatherDescriptionType.SHOWER_RAIN_AND_DRIZZLE.description,
        FiveDaysPageEnumWeatherDescriptionType.HEAVY_SHOWER_RAIN_AND_DRIZZLE.description,
        FiveDaysPageEnumWeatherDescriptionType.SHOWER_DRIZZLE.description -> R.drawable.ic_heavy_rain

        FiveDaysPageEnumWeatherDescriptionType.DUST_SAND.description,
        FiveDaysPageEnumWeatherDescriptionType.SAND.description,
        FiveDaysPageEnumWeatherDescriptionType.DUST.description -> R.drawable.ic_dust

        FiveDaysPageEnumWeatherDescriptionType.SQUALL.description -> R.drawable.ic_squalls

        FiveDaysPageEnumWeatherDescriptionType.TORNADO.description -> R.drawable.ic_tornado
        else -> R.drawable.ic_clear_sky
    }




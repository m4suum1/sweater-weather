package com.sweaterweather.detailpage

import android.os.Bundle
import android.view.View
import com.sweaterweather.R
import com.sweaterweather.common.mvp.BaseFragment
import com.sweaterweather.databinding.FragmentWeatherDetailedPageBinding
import com.sweaterweather.fivedayspage.ui.FiveDaysPageFragment
import com.sweaterweather.mainpage.model.WeatherData
import com.sweaterweather.utils.Arguments
import com.sweaterweather.utils.extensions.*
import com.sweaterweather.utils.extensions.viewbinding.viewBinding

class WeatherDetailedPageFragment : BaseFragment(R.layout.fragment_weather_detailed_page) {
    private val binding: FragmentWeatherDetailedPageBinding by viewBinding()

    companion object {
        fun newInstance(weatherData: WeatherData?) = WeatherDetailedPageFragment()
            .withArgs(Arguments.WEATHER_DATA to weatherData)
    }

    private val weatherData: WeatherData? by args(Arguments.WEATHER_DATA)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showDetailedWeather()

        with(binding) {
            toolBarDetailPageTitle.setNavigationOnClickListener {
                popScreen()
            }

            buttonFiveDaysInfo.setOnClickListener {
                val cityName = toolBarDetailPageTitle.title.toString()
                replaceScreen(FiveDaysPageFragment.newInstance(cityName))
            }
        }
    }

    private fun showDetailedWeather() {
        with(binding) {
            toolBarDetailPageTitle.title = weatherData?.name
            textViewFeelsLike.text = weatherData?.feelsLike
            textViewTempMin.text = weatherData?.tempMin
            textViewTempMax.text = weatherData?.tempMax
            textViewPressure.text = weatherData?.pressure
            textViewHumidity.text = weatherData?.humidity
        }
    }
}

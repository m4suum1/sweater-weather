package com.sweaterweather.common.mvp

interface MvpPresenter<V> {
    fun attachView(view: V)
    fun detachView()
}